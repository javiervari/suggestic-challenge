import ast

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from service.models import Item
from service.serializer import ItemSerializer

from service.flat import Flat


@api_view(['GET', 'POST'])
def item_list(request):
    if request.method == 'POST':
        try:
            request.data['items'] = str(Flat.flatten_data(data = request.data['items']))
        except Exception as e:
            return Response({'Error':str(e)}, status=400)
            
        serializer = ItemSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({'result':ast.literal_eval(request.data['items'])}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=400)

    elif request.method == 'GET':
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        