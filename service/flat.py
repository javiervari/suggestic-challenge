from collections.abc import Iterable


class Flat():
    output = []

    @staticmethod
    def flatten(obj):
        """
        Method to flatten.
        The method verifies if the paramether obj is an iterable object.
        In case obj is iterable: herself aply recursivity and it's re evaluate
        Else: obj is appended to the flat list.
        The method is recursive
        Parameters:
            obj (int, float, bool or str): 
                Object to be evaluated and converted in part of a flat list
        """
        if isinstance(obj, Iterable) and not isinstance(obj, str):
            for i in obj:
                Flat.flatten(i)
        else:
            Flat.output.append(obj)
            return


    @staticmethod
    def flatten_data(data):
        """
        Method to iterate over each data element and flatten it.

        Parameters:
            data (list): 
                Data to be flattened.

        Returns: 
            Output: A flat list with the inputdata elements no nested
        """
        try:

            for i in data:
                Flat.flatten(i)
            
            to_return = Flat.output
            Flat.output = []
            return to_return
        except Exception as e:
            raise Exception ("Object invalid to flatten. A list of int, float, bool, str is required")
