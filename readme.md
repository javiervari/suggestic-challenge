# Suggestic Challenge
Challenge requested by Suggestic company where I need create a web service using any framework that flattens a nested sequence into a single list of values.

ie:
Given the following entry:

```json
{
"items": [1, 2, [3, 4, [5, 6], 7], 8]
}
```
The following output is required:
```json
{
"result": [1, 2, 3, 4, 5, 6, 7, 8]
}
```

Extra points:
- Optionally save the result (Done!)
- Containerize


# Comments about the challenge!
- I used python v3 and Django Rest Framework for develop the challenge
- The extra point for save data is done.
- The only two endPoints are:
1. "localhost:8000/" [GET] : To list all elements flattened
2. "localhost:8000/" [POST] : To flatten a nested object

- I developed a class called `Flat()` to flatten the inputs. This class not only allow integer, also allows floating, strings and boolean elements in a nested list.
- The `Flat()` class raise an exception if something is wrong and the endpoint returns a json as:
```json
{"Error": "Object invalid to flatten. A list of int, float, bool, str is required"}
```

- I recommend test with inputs like these:

```json
{
"items":[
    [1,"ONE", false],
    2,
    [3,4,[5,6],7],
    8,
    [[1,"Hi", "World"], 3.14],
    [[[["Bye",5.55,["ABC", "DEF"]]],8.5],9],
    10
    ]
}
```
or
```json
{
"items":[
    1,
    2.25,
    [3,4,[5,6],7],
    8,
    [[50000,[[[[[[[[[777,"Test",999]]]]],true]]]]]]
    ]
}
```
...to validate and check my job.

- Included the requirements.txt



Thanks.
Javier A Valero
